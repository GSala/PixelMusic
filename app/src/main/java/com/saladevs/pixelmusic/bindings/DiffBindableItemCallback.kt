package com.saladevs.pixelmusic.bindings

import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList

/**
 *
 * Outlay2
 * DiffBindableItemCallback
 *
 * Created on 05/02/2018
 *
 *
 */
object DiffBindableItemCallback : DiffObservableList.Callback<DiffBindable> {
    override fun areItemsTheSame(oldItem: DiffBindable, newItem: DiffBindable): Boolean =
            oldItem.isItemTheSame(newItem)

    override fun areContentsTheSame(oldItem: DiffBindable, newItem: DiffBindable): Boolean =
            oldItem.areContentsTheSame(newItem)
}
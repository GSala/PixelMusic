package com.saladevs.pixelmusic.bindings

import android.databinding.BindingAdapter
import android.text.Html
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Flock2
 * ImageViewBindings
 *
 * Created on 01/07/2017
 *
 *
 */
object TextViewBindings {

    @JvmStatic
    @BindingAdapter("html")
    fun TextView.setHtml(html: String?) {
        text = Html.fromHtml(html ?: "").trim()
    }

    @JvmStatic
    @BindingAdapter("date")
    fun TextView.setDate(date: Date?) {
        val formatter = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        text = date?.let { formatter.format(date) }.orEmpty()
    }

}
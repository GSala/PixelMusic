package com.saladevs.pixelmusic.bindings

import android.databinding.BindingAdapter
import android.support.annotation.MenuRes
import android.support.v7.widget.Toolbar

/**
 *
 * Flock2
 * ImageViewBindings
 *
 * Created on 01/07/2017
 *
 *
 */
object ToolbarBindings {

    @JvmStatic
    @BindingAdapter("menuResource", "menuListener")
    fun setMenu(toolbar: Toolbar, @MenuRes menuResource: Int, menuItemClickListener: Toolbar.OnMenuItemClickListener) {
        toolbar.inflateMenu(menuResource)
        toolbar.setOnMenuItemClickListener(menuItemClickListener)
    }

}
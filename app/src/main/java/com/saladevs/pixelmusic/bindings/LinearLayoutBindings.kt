package com.saladevs.pixelmusic.bindings

import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.widget.LinearLayout

/**
 *
 * Flock2
 * ImageViewBindings
 *
 * Created on 01/07/2017
 *
 *
 */
object LinearLayoutBindings {

    @JvmStatic
    @BindingAdapter("items")
    fun bindFixedItems(linearLayout: LinearLayout, items: List<Bindable>) {
        val previousItems = (linearLayout.tag ?: emptyList<Any>()) as List<Any>
        if (items == previousItems) {
            for (index in items.indices) {
                val bindable = items[index]
                val binding = DataBindingUtil.getBinding<ViewDataBinding>(linearLayout.getChildAt(index))
                binding?.setVariable(bindable.bindingId, bindable)
            }
        } else {
            linearLayout.removeAllViews()
            for (index in items.indices) {
                val bindable = items[index]
                val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), bindable.layoutResource, linearLayout, true)
                binding.setVariable(bindable.bindingId, bindable)
            }
            linearLayout.tag = ArrayList(items)
        }
    }
}
package com.saladevs.pixelmusic.bindings

/**
 *
 * Outlay2
 * DiffBindable
 *
 * Created on 05/02/2018
 *
 *
 */
interface DiffBindable : Bindable {
    fun isItemTheSame(obj: Any): Boolean = false
    fun areContentsTheSame(obj: Any): Boolean = false
}
package com.saladevs.pixelmusic.bindings

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

/**
 *
 * Flock2
 * ImageViewBindings
 *
 * Created on 01/07/2017
 *
 *
 */
object ImageViewBindings {

    @JvmStatic
    @BindingAdapter("imageUrl", "circleCrop", requireAll = false)
    fun ImageView.setImageUrl(url: String?, circleCrop: Boolean = false) {
        Glide.with(context)
                .load(url)
                .apply { if (circleCrop) apply(RequestOptions.circleCropTransform()) }
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(this)
    }

    @JvmStatic
    @BindingAdapter("srcVector")
    fun setSrcVector(imageView: ImageView, @DrawableRes drawable: Int) {
        imageView.setImageResource(drawable)
    }
}
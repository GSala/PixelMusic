package com.saladevs.pixelmusic.bindings

import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.OnItemBind

/**
 *
 * Outlay2
 * DiffBindableItemBinding
 *
 * Created on 05/02/2018
 *
 *
 */
object BindableItemBinding : OnItemBind<Bindable> {

    override fun onItemBind(itemBinding: ItemBinding<*>, position: Int, item: Bindable) {
        itemBinding.set(item.bindingId, item.layoutResource)
    }
}
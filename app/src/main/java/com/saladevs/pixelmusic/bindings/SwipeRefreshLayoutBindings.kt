package com.saladevs.pixelmusic.bindings

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout

/**
 *
 * Flock2
 * ImageViewBindings
 *
 * Created on 01/07/2017
 *
 *
 */
object SwipeRefreshLayoutBindings {

    @JvmStatic
    @BindingAdapter("color")
    fun SwipeRefreshLayout.setMenu(color: Int) {
        setColorSchemeColors(color)
    }

}
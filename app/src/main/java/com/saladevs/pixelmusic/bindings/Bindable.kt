package com.saladevs.pixelmusic.bindings

import com.saladevs.pixelmusic.BR

/**
 *
 * Outlay2
 * Bindable
 *
 * Created on 05/02/2018
 *
 *
 */
interface Bindable {

    val bindingId: Int
        get() = BR.bindable

    val layoutResource: Int
}
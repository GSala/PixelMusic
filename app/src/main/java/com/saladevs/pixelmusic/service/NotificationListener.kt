package com.saladevs.pixelmusic.service

import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import com.saladevs.pixelmusic.App.Companion.context

/**
 *
 * PixelMusic
 * NotificationListener
 *
 * Created on 28/07/2018
 *
 *
 */
class NotificationListener : NotificationListenerService() {

    private var lastSong: String? = null

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        super.onNotificationPosted(sbn)

        if (sbn.packageName == MUSIC_PACKAGE_ID && sbn.notification.channelId == MUSIC_CHANNEL_ID) {
            val songName: String? = sbn.notification.extras.getString(EXTRA_TITLE_KEY)

            if (songName != null && songName != lastSong) {
                context.startService(SongMetadataService.fetchSongMetadataIntent(context, songName))
                lastSong = songName

            }
        }
    }

    companion object {
        private const val MUSIC_PACKAGE_ID = "com.google.intelligence.sense"
        private const val MUSIC_CHANNEL_ID = "com.google.intelligence.sense.ambientmusic.MusicNotificationChannel"
        private const val EXTRA_TITLE_KEY = "android.title"
    }
}
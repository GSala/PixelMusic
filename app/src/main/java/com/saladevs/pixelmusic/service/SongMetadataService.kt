package com.saladevs.pixelmusic.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.saladevs.pixelmusic.backend.SpotifyApi
import com.saladevs.pixelmusic.backend.entities.SearchResult
import com.saladevs.pixelmusic.db.DbManager
import com.saladevs.pixelmusic.db.Song
import timber.log.Timber
import java.time.LocalDateTime

/**
 *
 * PixelMusic
 * SongMetadataService
 *
 * Created on 28/07/2018
 *
 *
 */
class SongMetadataService : IntentService("SongMetadataService") {

    private val db = DbManager.db

    override fun onHandleIntent(intent: Intent) {
        Timber.d("Received intent")

        val songName = intent.getStringExtra(EXTRA_SONG_NAME)
                .split(" by ")
                .joinToString(" ")
        val searchResult: SearchResult = try {
            SpotifyApi.dataService.search(songName, "track").blockingGet()
        } catch (e: RuntimeException) {
            Timber.e(e.cause)
            null
        } ?: return

        val songResult = searchResult.trackSearchResults.tracks.firstOrNull() ?: return
        val title = songResult.name
        val artists = songResult.artists.map { it.name }.joinToString(", ")
        val imageUrl = songResult.album.images?.maxBy { it.width }?.url.orEmpty()
        val uri = songResult.uri

        db.songDao().insertSong(Song(LocalDateTime.now(), title, artists, imageUrl, uri))
    }

    companion object {

        private const val EXTRA_SONG_NAME = "extra_song"

        fun fetchSongMetadataIntent(context: Context, songName: String): Intent {
            val intent = Intent(context, SongMetadataService::class.java)
            intent.putExtra(EXTRA_SONG_NAME, songName)
            return intent
        }
    }
}
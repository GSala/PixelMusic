package com.saladevs.pixelmusic.db

import android.arch.persistence.room.Room
import com.saladevs.pixelmusic.App

/**
 *
 * PixelMusic
 * DbManager
 *
 * Created on 28/07/2018
 *
 *
 */
object DbManager {

    val db: AppDatabase by lazy {
        Room.databaseBuilder(App.context, AppDatabase::class.java, "database-name").build()
    }

}
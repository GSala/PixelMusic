package com.saladevs.pixelmusic.db

import android.arch.persistence.room.TypeConverter
import java.time.Instant
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.util.*

/**
 *
 * PixelMusic
 * Converters
 *
 * Created on 28/07/2018
 *
 *
 */
class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): LocalDateTime? {
        return if (value == null) null else LocalDateTime.ofInstant(Instant.ofEpochMilli(value), TimeZone.getDefault().toZoneId())
    }

    @TypeConverter
    fun dateToTimestamp(date: LocalDateTime?): Long? {
        return date?.toInstant(OffsetDateTime.now().offset)?.toEpochMilli()
    }
}


package com.saladevs.pixelmusic.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable

/**
 *
 * PixelMusic
 * SongDao
 *
 * Created on 28/07/2018
 *
 *
 */
@Dao
interface SongDao {

    @Query("SELECT * FROM song")
    fun getSongs(): Flowable<List<Song>>

    @Insert
    fun insertSong(song: Song)
}
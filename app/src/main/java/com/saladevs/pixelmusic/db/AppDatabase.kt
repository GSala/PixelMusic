package com.saladevs.pixelmusic.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

/**
 *
 * PixelMusic
 * AppDatabase
 *
 * Created on 28/07/2018
 *
 *
 */
@Database(entities = arrayOf(Song::class), version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun songDao(): SongDao
}
package com.saladevs.pixelmusic.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.time.LocalDateTime

/**
 *
 * PixelMusic
 * Song
 *
 * Created on 28/07/2018
 *
 *
 */
@Entity
@Parcelize
data class Song(
        @PrimaryKey val date: LocalDateTime,
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "artists") val artists: String,
        @ColumnInfo(name = "imageUrl") val imageUrl: String,
        @ColumnInfo(name = "uri") val uri: String
) : Parcelable
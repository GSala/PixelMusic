package com.saladevs.pixelmusic

import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 *
 * Outlay2
 * BaseFragment
 *
 * Created on 25/11/2017
 *
 *
 */
abstract class BaseFragment : Fragment() {

    abstract val binding: ViewDataBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            binding.root

}
package com.saladevs.pixelmusic.dashboard

import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import com.saladevs.pixelmusic.*
import com.saladevs.pixelmusic.bindings.DiffBindable
import com.saladevs.pixelmusic.bindings.DiffBindableItemBinding
import com.saladevs.pixelmusic.bindings.DiffBindableItemCallback
import com.saladevs.pixelmusic.databinding.DashboardFragmentBinding
import com.saladevs.pixelmusic.db.Song
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import timber.log.Timber

class DashboardFragment : BaseFragment() {

    private val model: DashboardViewModel by viewModelProvider { DashboardViewModel() }

    override val binding: DashboardFragmentBinding
            by bindingProvider(R.layout.dashboard_fragment, { BR.bindable to this })

    val items = DiffObservableList(DiffBindableItemCallback)
    val itemBinding = DiffBindableItemBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model.songs.observe(this, Observer {
            Timber.d("Received songs - Size : ${it?.size}")
            val bindables = mutableListOf<DiffBindable>()

            it?.sortedByDescending { it.date }
                    ?.groupBy {
                        it.date.withMinute((it.date.minute / 15) * 15).withSecond(0).withNano(0)
                    }
                    ?.forEach { i, list ->
                        bindables.add(TimeHeaderBindable(i))
                        bindables.addAll(list.map { SongBindable(it, ::onSongClick) })
                    }

            items.update(bindables)
        })
    }

    fun onSongClick(song: Song) {
        Timber.d("onSongClick")
        val intent = Intent(ACTION_VIEW, Uri.parse(song.uri))
        startActivity(intent)
    }
}

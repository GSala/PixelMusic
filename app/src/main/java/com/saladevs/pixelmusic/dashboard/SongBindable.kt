package com.saladevs.pixelmusic.dashboard

import android.view.View
import com.saladevs.pixelmusic.R
import com.saladevs.pixelmusic.bindings.DiffBindable
import com.saladevs.pixelmusic.db.Song

/**
 *
 * PixelMusic
 * SongBindable
 *
 * Created on 28/07/2018
 *
 *
 */
data class SongBindable(val song: Song, val onSongClick: (Song) -> Unit) : DiffBindable {

    override val layoutResource = R.layout.dashboard_song_bindable

    val title = song.title
    val artists = song.artists
    val imageUrl = song.imageUrl

    fun onClick(view: View) {
        onSongClick(song)
    }

    override fun isItemTheSame(obj: Any): Boolean {
        return obj is SongBindable && obj.song.date == song.date
    }

    override fun areContentsTheSame(obj: Any): Boolean {
        return obj is SongBindable && obj.song == song
    }
}
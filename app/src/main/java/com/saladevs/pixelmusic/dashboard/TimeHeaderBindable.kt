package com.saladevs.pixelmusic.dashboard

import com.saladevs.pixelmusic.R
import com.saladevs.pixelmusic.bindings.DiffBindable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 *
 * PixelMusic
 * TimeHeaderBindable
 *
 * Created on 29/07/2018
 *
 *
 */
data class TimeHeaderBindable(val date: LocalDateTime) : DiffBindable {

    override val layoutResource = R.layout.dashboard_header_time_bindable

    val text = date.format(DateTimeFormatter.ofPattern("dd-MM, HH:mm"))

    override fun isItemTheSame(obj: Any): Boolean {
        return obj is TimeHeaderBindable && date == obj.date
    }

    override fun areContentsTheSame(obj: Any): Boolean {
        return true
    }
}
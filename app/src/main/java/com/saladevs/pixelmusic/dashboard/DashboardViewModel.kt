package com.saladevs.pixelmusic.dashboard

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.saladevs.pixelmusic.addTo
import com.saladevs.pixelmusic.db.DbManager
import com.saladevs.pixelmusic.db.Song
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 *
 * PixelMusic
 * DashboardViewModel
 *
 * Created on 28/07/2018
 *
 *
 */
class DashboardViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    private val db = DbManager.db

    val songs: MutableLiveData<List<Song>> = MutableLiveData()

    init {
        db.songDao().getSongs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    songs.value = it
                }, { Timber.e(it) })
                .addTo(disposables)
    }

    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}
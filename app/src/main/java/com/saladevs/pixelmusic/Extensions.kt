package com.saladevs.pixelmusic

import android.app.Activity
import android.arch.lifecycle.*
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.widget.Toast
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.reactivestreams.Publisher

/**
 *
 * Outlay2
 * Extensions
 *
 * Created on 25/11/2017
 *
 *
 */

fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun <T> Publisher<T>.toLiveData(): LiveData<T> {
    return LiveDataReactiveStreams.fromPublisher(this)
}

inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(crossinline provider: () -> VM) = lazy {
    val factory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>) =
                provider() as T
    }
    ViewModelProviders.of(this, factory).get(VM::class.java)
}

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(crossinline provider: () -> VM) = lazy {
    val factory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>) =
                provider() as T
    }
    ViewModelProviders.of(activity!!, factory).get(VM::class.java)
}

inline fun <reified BINDING : ViewDataBinding> Activity.bindingProvider(layoutId: Int, vararg variables: Pair<Int, Any>) = lazy {
    DataBindingUtil.inflate<BINDING>(layoutInflater, layoutId, null, false)
            .also { for (v in variables) it.setVariable(v.first, v.second) }
}

inline fun <reified BINDING : ViewDataBinding> Fragment.bindingProvider(layoutId: Int, vararg variables: () -> Pair<Int, Any>) = lazy {
    DataBindingUtil.inflate<BINDING>(layoutInflater, layoutId, null, false)
            .also { for (v in variables) v().run { it.setVariable(first, second) } }
}

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

inline fun Boolean.ifTrue(block: () -> Unit) {
    if (this) block()
}

inline fun Boolean.ifFalse(block: () -> Unit) {
    if (!this) block()
}



package com.saladevs.pixelmusic

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import timber.log.Timber

/**
 *
 * PixelMusic
 * App
 *
 * Created on 28/07/2018
 *
 *
 */
class App : Application() {

    companion object {

        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        context = this

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * Track
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class TrackSearchResults(
        @Json(name = "items") val tracks: List<Track>
)
package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * Track
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class Track(
        @Json(name = "name") val name: String,
        @Json(name = "album") val album: Album,
        @Json(name = "artists") val artists: List<Artist>,
        @Json(name = "uri") val uri: String
)
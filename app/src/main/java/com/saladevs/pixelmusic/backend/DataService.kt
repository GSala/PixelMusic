package com.saladevs.pixelmusic.backend

import com.saladevs.pixelmusic.backend.entities.SearchResult
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * PixelMusic
 * DataService
 *
 * Created on 28/07/2018
 *
 *
 */
interface DataService {

    @GET("search")
    fun search(
            @Query("q") query: String,
            @Query("type") type: String,
            @Query("market") market: String = "DK",
            @Query("limit") limit: Int = 5
    ): Single<SearchResult>
}
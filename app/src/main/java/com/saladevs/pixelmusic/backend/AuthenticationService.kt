package com.saladevs.pixelmusic.backend

import com.saladevs.pixelmusic.backend.entities.auth.AuthTokenResponse
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 *
 * PixelMusic
 * DataService
 *
 * Created on 28/07/2018
 *
 *
 */
interface AuthenticationService {

    @FormUrlEncoded
    @POST("token")
    fun getAuthorizationToken(@Field("grant_type") type: String): Single<AuthTokenResponse>

}
package com.saladevs.pixelmusic.backend.entities.auth

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * AuthTokenResponse
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class AuthTokenResponse(
        @Json(name = "access_token") val accessToken: String,
        @Json(name = "token_type") val tokenType: String,
        @Json(name = "expires_in") val expiresIn: Int,
        @Json(name = "scope") val scope: String?
)
package com.saladevs.pixelmusic.backend

import com.saladevs.pixelmusic.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

/**
 *
 * PixelMusic
 * SpotifyApi
 *
 * Created on 28/07/2018
 *
 *
 */
object SpotifyApi {

    private val moshi = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .build()

    private val loggingInterceptor = HttpLoggingInterceptor()
            .apply { level = HttpLoggingInterceptor.Level.BASIC }

    private val authClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        .header("Authorization", Credentials.basic(BuildConfig.SPOTIFY_CLIENT_ID, BuildConfig.SPOTIFY_CLIENT_SECRET))
                        .build()
                chain.proceed(request)
            }.build()

    private val authRetrofit = Retrofit.Builder()
            .client(authClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://accounts.spotify.com/api/")
            .build()

    private val authenticator = object : Authenticator {
        override fun authenticate(route: Route?, response: Response): Request? {
            if (response.request().header("Authorization") != null) {
                return null // Give up, we've already attempted to authenticate.
            }

            System.out.println("Authenticating for response: $response")
            System.out.println("Challenges: " + response.challenges())
            val authorizationToken = authService.getAuthorizationToken("client_credentials").blockingGet()
            val credential = "Bearer " + authorizationToken.accessToken
            return response.request().newBuilder()
                    .header("Authorization", credential)
                    .build()
        }
    }

    private val dataHttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .authenticator(authenticator)
            .build()

    private val dataRetrofit = Retrofit.Builder()
            .client(dataHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.spotify.com/v1/")
            .build()

    val authService = authRetrofit.create(AuthenticationService::class.java)
    val dataService = dataRetrofit.create(DataService::class.java)

}
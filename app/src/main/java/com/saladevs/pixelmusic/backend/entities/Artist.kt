package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * Album
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class Artist(
        @Json(name = "external_urls") val externalUrls: Map<String, String>?,
        @Json(name = "followers") val followers: List<Any>?,
        @Json(name = "genres") val genres: List<String>?,
        @Json(name = "href") val href: String?,
        @Json(name = "id") val id: String?,
        @Json(name = "images") val images: List<Image>?,
        @Json(name = "name") val name: String,
        @Json(name = "popularity") val popularity: Int?,
        @Json(name = "type") val type: String?,
        @Json(name = "uri") val uri: String?
)
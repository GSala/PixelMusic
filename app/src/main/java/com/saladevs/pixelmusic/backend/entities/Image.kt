package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * Image
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class Image(
        @Json(name = "width") val width: Int,
        @Json(name = "height") val height: Int,
        @Json(name = "url") val url: String
)
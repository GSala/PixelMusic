package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * SearchResult
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class SearchResult(
        @Json(name = "tracks") val trackSearchResults: TrackSearchResults
)
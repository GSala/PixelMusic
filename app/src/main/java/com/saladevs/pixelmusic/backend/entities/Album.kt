package com.saladevs.pixelmusic.backend.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 * PixelMusic
 * Album
 *
 * Created on 28/07/2018
 *
 *
 */
@JsonClass(generateAdapter = true)
data class Album(
        @Json(name = "album_type") val albumType: String?,
        @Json(name = "artists") val artists: List<Artist> = emptyList(),
        @Json(name = "available_markets") val availableMarkets: List<String>?,
        @Json(name = "external_urls") val externalUrls: Map<String, String>?,
        @Json(name = "href") val href: String?,
        @Json(name = "id") val id: String?,
        @Json(name = "images") val images: List<Image>?,
        @Json(name = "name") val name: String?,
        @Json(name = "type") val type: String?,
        @Json(name = "uri") val uri: String?
)